package org.tuxship.jlatex;

import java.io.File;
import java.io.IOException;

/**
 * Created by Matthias Ervens on 11/07/17.
 */
public class JLatex {

    public static JLDocument createDocument(String texTemplate, File workdir) {
        return createDocument(texTemplate, workdir,  "JLatex Template");
    }

    public static JLDocument createDocument(String texTemplate, File workdir, String name) {
        return new JLDocument(texTemplate, workdir, name);
    }

    public static File toPdf(JLDocument document, File pdfLatex) throws IOException {
        return JLPdfGenerator.createPdf(document, pdfLatex, 2);
    }

    public static File toPdf(JLDocument document, File pdfLatex, int iterations) throws IOException {
        return JLPdfGenerator.createPdf(document, pdfLatex, iterations);
    }
}
