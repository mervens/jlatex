package org.tuxship.jlatex.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Matthias Ervens on 19.07.2017.
 */
public class Stopwatch {

    private static final Logger logger = LoggerFactory.getLogger(Stopwatch.class);

    private static Map<String, Long> watches;

    public static void start(String id) {
        if(watches == null) watches = new HashMap<>(8);
        watches.put(id, System.currentTimeMillis());
    }

    public static void stop(String id) {
        Long stopped = System.currentTimeMillis();

        if(!watches.containsKey(id))
            logger.error("No Stopwatch with that id! ('{}')", id);
        Long started = watches.get(id);

        logger.trace("{} took: {} ms", id, stopped - started);
    }
}
