package org.tuxship.jlatex;

import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by Matthias Ervens on 11/07/17.
 */
public class JLDocument {

    private static final Logger logger = LoggerFactory.getLogger(JLDocument.class);

    private VelocityContext contextData;

    private String name;
    private String template;
    private File workdir;

    public JLDocument(String template, File workdir, String name) {
        this.name = name;
        this.template = template;
        this.workdir = workdir;

        this.contextData = new VelocityContext();
    }

    public void set(String key, Object value) {
        contextData.put(key, value);
    }

    public String getName() {
        return name;
    }

    String getTemplate() {
        return template;
    }

    File getWorkdir() {
        return workdir;
    }

    VelocityContext getContextData() {
        return contextData;
    }
}
