package org.tuxship.jlatex;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tuxship.jlatex.util.Stopwatch;

import java.io.*;
import java.nio.charset.Charset;

/**
 * Created by Matthias Ervens on 11/07/17.
 */
class JLPdfGenerator {

    private static final Charset UTF8 = Charset.forName("UTF-8");

    private static final Logger logger = LoggerFactory.getLogger(JLPdfGenerator.class);

    private static final Object velocityLock = 1648314;

    private static VelocityEngine velocityEngine;

    private static void initEngine() {
        velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(VelocityEngine.RUNTIME_LOG_LOGSYSTEM, new SLF4JLogChute());
        velocityEngine.setProperty("runtime.log.logsystem.slf4j.logger", "org.apache.velocity");
        try {
            velocityEngine.init();
        } catch (Exception e) {
            logger.error("Exception while initializing the VelocityEngine!", e);
        }
    }

    static File createPdf(JLDocument document, File pdfLatex, int iterations) throws IOException {
        logger.trace("Generating PDF with JLatex!");
        synchronized (velocityLock) {
            if (velocityEngine == null) initEngine();
        }

        Stopwatch.start("Velocity");
        // Convert the document's template into a complete latex document
        File texOutFile = new File(document.getWorkdir(), document.getName() + ".tex");

        BufferedReader templateIn = new BufferedReader(new StringReader(document.getTemplate()));
        BufferedWriter texOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(texOutFile), UTF8));

        if (!velocityEngine.evaluate(document.getContextData(), texOut, document.getName(), templateIn)) {
            logger.error("Velocity encountered an error while evaluating the input.");
            templateIn.close();
            texOut.flush();
            texOut.close();
            return null;
        }

        templateIn.close();
        texOut.flush();
        texOut.close();
        Stopwatch.stop("Velocity");

        // Generate a PDF from the latex document.
        File pdfOutFile = new File(document.getWorkdir(), document.getName() + ".pdf");

        Stopwatch.start("Pdflatex");
        if(!generate(pdfLatex, iterations, texOutFile, document.getWorkdir(), pdfOutFile)) {
            return null;
        }
        Stopwatch.stop("Pdflatex");

        return pdfOutFile;
    }

    private static boolean generate(File pdfLatex, int iterations, File texFile, File workdir, File pdfOutFile) throws IOException {
        if(!verifyInput(iterations, texFile, workdir)) return false;

        String log;

        ProcessBuilder processBuilder = new ProcessBuilder(
                pdfLatex.getCanonicalPath(),
                "--interaction=nonstopmode",
                "--output-directory=" + workdir.getCanonicalPath(),
                texFile.getCanonicalPath()
        );
        processBuilder.redirectErrorStream(true);
        processBuilder.directory(workdir);

        StringBuilder cmdString = new StringBuilder();
        processBuilder.command().forEach(s -> cmdString.append(s).append(" "));
        logger.debug("Running latex with: " + cmdString.toString());

        for(int i = 1; i <= iterations; i++) {
            Stopwatch.start("pdflatex_" + i);
            Process process = processBuilder.start();

            // read pdfLatex's log
            try(BufferedReader logReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                StringBuilder logBuilder = new StringBuilder();
                String line;
                while ((line = logReader.readLine()) != null) {
                    logBuilder.append(line).append(System.lineSeparator());
                }

                log = logBuilder.toString();
            }

            // wait for pdfLatex to finish
            try {
                int pExit = process.waitFor();
                if(pdfOutFile == null || !pdfOutFile.isFile()) {
                    logger.error("PDF file could not be created.");
                    logger.error(log);
                    return false;
                }

                if(pExit != 0) {
                    logger.error("pdfLatex finished with non-zero value ({})!", pExit);
                    logger.error(log);
                    return false;
                }

            } catch (InterruptedException e) {
                logger.error("Exception while executing pdfLatex!", e);
                logger.error(log);
                return false;
            }
            Stopwatch.stop("pdflatex_" + i);
        }

        return true;
    }

    private static boolean verifyInput(int iterations, File texFile, File workdir) {
        boolean valid = true;

        if(iterations <= 0) {
            logger.error("Iterations has to be a positive and non-zero integer.");
            valid = false;
        }

        if(texFile == null || !texFile.isFile()) {
            logger.error("Latex input file does not exist.");
            valid = false;
        }

        if(workdir == null || !workdir.isDirectory()) {
            logger.error("Working directory does not exist.");
            valid = false;
        }

        return valid;
    }
}
